var app = angular.module('demo', ['elasticui','ngSanitize']);

app.constant('euiHost', 'http://localhost:9200');
app.controller('IndexController', function($scope) {
    $scope.indexName = "hotel";
    $scope.getFacilityIcon= function($facility){
        switch ($facility){
            case 'Bar':

        }

    }

    $scope.getTimes=function(n){
        return new Array(parseInt(n));
    };

    $scope.getNumber = function(n){//console.log(n);
        return parseInt(n);
    }


});

app.filter('icon', function($sce){
        return function(item, data, color){

            if(!color){
                color = '';
            }

            switch(item){
                case 'USD' : return $sce.trustAsHtml("<i class='fa fa-usd "+ color +"'></i> ") ;break;
                case 'ZAR' : return $sce.trustAsHtml("<i class='"+ color +"'>R</i> ");break;
                case 'XOF' : return $sce.trustAsHtml("<i class='"+ color +"'>CHF</i> ");break;
                case 'NGN' : return $sce.trustAsHtml("<strike><i class='"+ color +"'>N</i> </strike>");break;
                case 'NAD' : return $sce.trustAsHtml("<i class='fa fa-usd "+ color +"'></i> ") ;break;
                case 'MZN' : return $sce.trustAsHtml("<i class='"+ color +"'>MT</i> ");break;
                case 'EUR' : return $sce.trustAsHtml("<i class='fa fa-eur "+ color +"'></i> ") ;break;
                case 'BWP' : return $sce.trustAsHtml("<i class=' "+ color +"'><b>P</b></i> ") ;break;
                default : return item ;break;
            }
        };
    });

